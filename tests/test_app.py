"""
Basic integration test that uses fake HDB++ devices, but needs a real Tango DB.
"""

from multiprocessing import Process
import os
import socket
import time
from uuid import uuid4

import mechanize
import uvicorn
import pytest
import tango

from archwizard import main

from .fake_hdb import HdbConfigurationManager, HdbEventSubscriber


ID = uuid4()
HDB_MANAGER = f"archwizard/manager/{ID}"


def get_open_port():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("", 0))
    s.listen(1)
    port = s.getsockname()[1]
    s.close()
    return port


def make_server_fixture(server, devices):

    """ Return a pytest fixture providing a Tango device server. """

    def fixture():
        db = tango.Database()
        devinfos = []
        devclasses = set()
        for device, config in devices.items():
            devinfo = tango.DbDevInfo()
            devinfo.name = device
            devinfo._class = config["class"].__name__
            devinfo.server = server
            devinfos.append(devinfo)
            devclasses.add(config["class"])
        db.add_server(devinfo.server, devinfos, with_dserver=True)
        for device, config in devices.items():
            props = config.get("properties")
            if props:
                db.put_device_property(device, props)
        proc = Process(target=tango.server.run,
                       kwargs={
                           "classes": tuple(devclasses),
                           "args": server.split("/")
                       },
                       daemon=False)
        proc.start()
        proxies = [tango.DeviceProxy(device) for device in devices]
        for proxy in proxies:
            while True:
                try:
                    proxy.ping()
                    break
                except tango.DevFailed:
                    time.sleep(0.1)

        yield proxies

        proc.terminate()
        db.delete_server(devinfo.server)

    return pytest.fixture(fixture, scope="module")


@pytest.fixture(scope="module")
def archwizard():
    tango_host = tango.ApiUtil.get_env_var("TANGO_HOST")
    os.environ["HDB_CONFIGURATION_MANAGERS"] = \
        f"testing=tango://{tango_host}/{HDB_MANAGER}"
    port = get_open_port()
    proc = Process(target=uvicorn.run,
                   kwargs={
                       "app": main.app,
                       "host": "127.0.0.1",
                       "port": port,
                       "log_level": "info"},
                   daemon=False)
    proc.start()
    time.sleep(1)  # TODO improve
    yield f"http://localhost:{port}"
    proc.kill()


TANGO_HOST = tango.ApiUtil.get_env_var("TANGO_HOST")

HDB_ARCHIVERS = [
    f"archwizard/archiver/{ID}-{i}"
    for i in range(10)
]

HDB_ATTRIBUTES = {
    archiver: [
        f"archwizard/device/{ID}-{j}/attribute-{j}"
        for j in range(10)
    ]
    for archiver in HDB_ARCHIVERS
}


hdb_manager = make_server_fixture(
    f"HdbConfigurationManager/{ID}",
    {
        HDB_MANAGER: {
            "class": HdbConfigurationManager,
            "properties": {
                "ArchiverList": [
                    f"tango://{TANGO_HOST}/{archiver}"
                    for archiver in HDB_ARCHIVERS
                ]
            }
        },
    })

hdb_archivers = make_server_fixture(
    f"HdbEventSubscriber/{ID}",
    {
        archiver: {
            "class": HdbEventSubscriber,
            "properties": {
                "AttributeList": [
                    f"tango://{TANGO_HOST}/{attribute}"
                    for attribute in HDB_ATTRIBUTES[archiver]
                ]
            }
        }
        for archiver in HDB_ARCHIVERS
    })


def test_index(archwizard, hdb_manager, hdb_archivers):
    br = mechanize.Browser()
    br.open(archwizard)
    assert br.viewing_html()

    br.follow_link(name="testing")
    assert br.viewing_html()
    assert HDB_MANAGER in br.title()


def test_manager(archwizard, hdb_manager, hdb_archivers):
    br = mechanize.Browser()
    br.open(archwizard + "/manager/testing")
    assert br.viewing_html()

    # We have links to all the archivers
    links = br.links()
    unlinked_archivers = set(HDB_ARCHIVERS)
    archiver_links = []
    for link in links:
        if "/archiver" in link.url:
            archiver_links.append(link)
            for archiver in HDB_ARCHIVERS:
                if archiver in link.text:
                    unlinked_archivers.remove(archiver)
    assert not unlinked_archivers


def test_manager_search_attribute(archwizard, hdb_manager, hdb_archivers):
    br = mechanize.Browser()
    br.open(archwizard + "/manager/testing")
    br.select_form(name="attribute-search")

    # Note: fake manager always returns first attribute of first archiver when searching
    br.form["attr_search"] = "I can write whatever here"
    br.submit()

    # Click a search result
    archiver = HDB_ARCHIVERS[0]
    attribute = HDB_ATTRIBUTES[archiver][0]
    br.follow_link(text=attribute)
    assert br.viewing_html()
    assert attribute in br.title()


def test_manager_search_attribute_archiver_link(archwizard, hdb_manager, hdb_archivers):
    br = mechanize.Browser()
    br.open(archwizard + "/manager/testing")
    br.select_form(name="attribute-search")

    br.form["attr_search"] = "Does not matter"
    br.submit()

    # Click a search result's archiver link
    br.follow_link(text="Link")
    assert br.viewing_html()
    archiver = HDB_ARCHIVERS[0]
    assert archiver in br.title()


def test_archiver(archwizard, hdb_manager, hdb_archivers):
    br = mechanize.Browser()
    archiver = HDB_ARCHIVERS[0]
    br.open(archwizard + f"/manager/testing/archiver?archiver={archiver}")
    assert br.viewing_html()
    assert archiver in br.title()

    # Check that we link to all attributes
    links = br.links()
    attributes = HDB_ATTRIBUTES[archiver]
    unlinked_attributes = set(attributes)
    attribute_links = []
    for link in links:
        if "/attribute" in link.url:
            attribute_links.append(link)
            for attribute in attributes:
                if attribute in link.text:
                    unlinked_attributes.remove(attribute)
    assert not unlinked_attributes


def test_archiver_attribute_link(archwizard, hdb_manager, hdb_archivers):
    br = mechanize.Browser()
    archiver = HDB_ARCHIVERS[0]
    br.open(archwizard + f"/manager/testing/archiver?archiver={archiver}")
    assert br.viewing_html()
    assert archiver in br.title()

    # Check that attribute links work
    attribute = HDB_ATTRIBUTES[archiver][0]
    br.follow_link(text_regex="/attribute")
    assert attribute in br.title()


def test_attribute_archiver_link(archwizard, hdb_manager, hdb_archivers):
    br = mechanize.Browser()
    archiver = HDB_ARCHIVERS[0]
    attribute = HDB_ATTRIBUTES[archiver][0]
    br.open(archwizard + f"/manager/testing/attribute?attribute={attribute}")
    assert br.viewing_html()
    assert attribute in br.title()

    # Check that link back to archiver works
    br.follow_link(text_regex="/archiver")
    assert archiver in br.title()
