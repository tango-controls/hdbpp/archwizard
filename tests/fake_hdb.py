from collections import defaultdict
import json
from random import random, randint, sample

import tango
from tango.server import Device, attribute, command, device_property


class HdbConfigurationManager(Device):

    """
    A device that looks like a HDB++ manager, but in fact does nothing
    except record commands being run.
    """

    archiverlist = device_property(dtype=[str], default_value=[])

    @attribute(dtype=[str], max_dim_x=100)
    def ArchiverList(self):
        return self.archiverlist

    @attribute(dtype=[str], max_dim_x=100)
    def ArchiverStatus(self):
        return [
            f"{archiver}\tON\t\tNOK=0/3\tPending=0"
            for archiver in self.archiverlist
        ]

    @attribute(dtype=int)
    def AttributeNumber(self):
        return 14

    @attribute(dtype=int)
    def AttributeNokNumber(self):
        return 3

    @attribute(dtype=int)
    def AttributeStoppedNumber(self):
        return 1

    @attribute(dtype=int)
    def AttributePendingNumber(self):
        return 0

    @attribute(dtype=float)
    def AttributeRecordFreq(self):
        return 12.3

    @attribute(dtype=float)
    def AttributeFailureFreq(self):
        return 0

    @attribute(dtype=float)
    def AttributeMaxStoreTime(self):
        return 0.32

    @attribute(dtype=float)
    def AttributeMinStoreTime(self):
        return 0.32

    @attribute(dtype=float)
    def AttributeMaxProcessingTime(self):
        return 0.021

    @attribute(dtype=float)
    def AttributeMinProcessingTime(self):
        return 0.012

    # ========== Commands ==========
    @command(dtype_in=str, dtype_out=[str])
    def AttributeSearch(self, query):
        proxy = tango.DeviceProxy(self.archiverlist[0])
        attributes = proxy.AttributeList
        return [attributes[0]]

    @command(dtype_in=str, dtype_out=str)
    def AttributeGetArchiver(self, attr):
        return self.archiverlist[0]


class HdbEventSubscriber(Device):

    attributelist = device_property(dtype=[str])

    @attribute(dtype=[str], max_dim_x=100)
    def AttributeList(self):
        return self.attributelist

    @command(dtype_in=str, dtype_out=str)
    def AttributeStatus(self, attr):
        return "\n".join([
            "Event status       : Event received",
            "Events engine      : ZMQ",
            "Archiving          : Started",
            "Event OK counter   : 1732 - 2021-09-18 17:44:54.241246",
            "Event NOK counter  : 2558 - 2021-09-18 17:35:20.528036",
            "DB ERRORS counter  : 0 - YYYY-MM-DD HH:MM:SS.UUUUUU",
            "Storing time AVG   : 0.009101s",
            "Processing time AVG: 0.009187s",
        ])


class DummyDevice(Device):

    @attribute(dtype=float)
    def apa(self):
        return random()

    @attribute(dtype=float)
    def bepa(self):
        return random()

    @attribute(dtype=float)
    def cepa(self):
        return random()

    @attribute(dtype=float)
    def depa(self):
        return random()
