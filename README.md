ArchWizard 🧙
=============

"ArchWizard" is a simple web interface to the HDB++ archiving system.

It is intended for monitoring and troubleshooting, not maintenance such as adding and removing attributes.

It's built using Python, FastAPI, Jinja2 and PyTango.


Features
---------
- Display archiving statistics and errors
- Searching on attributes
- Filtering on archivers, attribute name and status (NOK, stopped, etc)
- Temporarily starting and stopping archiving of individual attributes


Usage
-----

Note that archwizard needs to be able to directly access the Tango control system(s), where HDB++ is set up with a configuration manager and event subscribers.

Using virtualenv + pip:

    $ python3 -m venv env
    $ env/bin/pip install -e .
    $ env/bin/uvicorn archwizard.main:app
    
ArchWizard should now be reachable on http://localhost:8000/

There is also a conda environment file: `environment.yml`, as well as an example Docker file (see below).


Docker
------

An example docker file is included.

If you are not within the MAX IV network, you will have to tweak it to replace the internal MAX IV repo URLs. Also set your timezone.

    $ docker build . -t archwizard
    $ docker run -d -p 8000:8000 archwizard

To configure the service, you can put configuration varibles in a file (see below) and specify it using the `--envfile` argument to `docker run`. You may also need to modify the docker image a bit to fit your situation.


Configuration
--------------

You can tell ArchWizard which HdbConfigurationManager device(s) to work through. This can be provided as an environment variable, e.g: 

    HDB_CONFIGURATION_MANAGERS=MyHDB=tango://my-tango-host:10000/hdb/manager/1,OtherHDB=tango://...
    
Here, "MyHDB", "OtherHDB" might be an informal name of the control system, perhaps e.g. the beamline name. It will be visible to a visitor.

You can also put this in a file called `.env` in the directory where ArchWizard is run. 

If this information is not provided, ArchWizard will use the first HDB++ manager device it finds in the local Tango control system (if any).

Note: the `deployment-configs` directory contains helm settings specific to the OKD deployment at MAX IV.
