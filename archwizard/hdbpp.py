"""
Stuff for talking to HDB++.
"""

import logging
import re
from typing import NamedTuple

import tango
from tango.asyncio import DeviceProxy


class ArchiverInfo(NamedTuple):
    name: str
    state: str
    total: int
    nok: int
    pending: int
    error: str = ""

    @property
    def alarm(self):
        return self.state == "ALARM"

    @property
    def fault(self):
        return self.state == "FAULT"

    @property
    def broken(self):
        return bool(self.error)


class AttributeInfo(NamedTuple):
    name: str
    error: str
    events: int
    pending: int
    record_freq: float
    started: bool
    stopped: bool
    paused: bool
    nok: bool

    def shortname(self):
        return shorten_attribute_name(self.name)

    def device(self):
        return split_attribute_name(self.name)[0]

    def attribute(self):
        return split_attribute_name(self.name)[1]

    @property
    def status(self):
        return ((self.nok and "NOK") or (self.stopped and "Stopped") or "")


PREFIX_FORMAT = r"tango://[^:]+:\d+/"
DEVICE_FORMAT = r"[\-\w.@]+/[\-\w.@]+/[\-\w.@]+"
ATTRIBUTE_FORMAT = r"[\-\w.@]+"

ARCHIVER_FORMAT = rf"({PREFIX_FORMAT})?({DEVICE_FORMAT})"
FULL_ATTRIBUTE_FORMAT = rf"({PREFIX_FORMAT})?({DEVICE_FORMAT})/({ATTRIBUTE_FORMAT})"


def shorten_archiver_name(name):
    match = re.match(ARCHIVER_FORMAT, name)
    return match.groups()[-1]


def shorten_attribute_name(name):
    return "/".join(split_attribute_name(name))


def split_attribute_name(name):
    match = re.match(FULL_ATTRIBUTE_FORMAT, name)
    return match.groups()[-2:]


def prefix_if_missing(prefix, device):
    if re.match(rf"({PREFIX_FORMAT})({DEVICE_FORMAT})", device):
        return device
    return prefix + device


class HdbppConfig:

    "Simple interface to a HDB++ installation"

    def __init__(self, manager_device):
        self.manager_device = manager_device.lower()
        match = re.match(ARCHIVER_FORMAT, self.manager_device)
        self.prefix = match.group(1)

    async def setup(self):
        self.manager = await DeviceProxy(self.manager_device)
        self.db = await DeviceProxy(prefix_if_missing(self.prefix, "sys/database/2"))
        archiver_devices = await self.manager.read_attribute("ArchiverList")
        self.archivers = {}
        for archiver in archiver_devices.value:
            try:
                proxy = await DeviceProxy(prefix_if_missing(self.prefix, archiver))
                norm_name = prefix_if_missing(self.prefix, archiver).lower()
                self.archivers[norm_name] = proxy
            except tango.DevFailed as e:
                # The archiver does not exist?
                logging.error("Could not reach archiver %r for manager %r: %s",
                              archiver, self.manager_device, e.args[-1].desc)
                raise

    def get_archiver_proxy(self, archiver):
        logging.debug("get_archiver_proxy %r %r", archiver, prefix_if_missing(self.prefix, archiver))
        return self.archivers.get(prefix_if_missing(self.prefix, archiver).lower())

    async def get_device_info(self, device):
        _, result = await self.db.command_inout("DbGetDeviceInfo",
                                                shorten_archiver_name(device))
        return {
            "started_date": list(result)[5]
            # TODO add more, this is the only one currently in use
        }

    async def get_manager_info_attributes(self):
        info_attributes = [
            "Status",
            "ArchiverStatus",
            "AttributeFailureFreq",
            "AttributeMaxPendingNumber",
            "AttributeMaxProcessingTime",
            "AttributeMaxStoreTime",
            "AttributeMinPendingNumber",
            "AttributeMinProcessingTime",
            "AttributeMinStoreTime",
            "AttributeNokNumber",
            "AttributeNumber",
            "AttributeRecordFreq",
            "AttributeStoppedNumber",
            "AttributePendingNumber"
        ]
        info = await self.manager.read_attributes(info_attributes)
        return dict(zip(info_attributes, (attr.value for attr in info)))

    async def search_attributes(self, search: str):
        return await self.manager.command_inout("AttributeSearch", search)

    async def get_archiver_for_attribute(self, attribute):
        full_attr = prefix_if_missing(self.prefix, attribute)
        return await self.manager.command_inout("AttributeGetArchiver", full_attr)

    async def get_archiver_status(self, archiver):
        proxy = self.get_archiver_proxy(archiver)
        status = (await proxy.read_attribute("Status")).value
        return status

    def _parse_archiver_status(self, status: str):
        archiver, rest = status.split(maxsplit=1)
        if rest.startswith("Error="):
            state = n_nok = n_pending = n_total = None
            error = rest[6:].strip("'")
        else:
            archiver, state, nok, pending = status.split()
            error = None
            nok_re = r"NOK=(\d+)/(\d+)"
            match = re.match(nok_re, nok)
            n_nok = int(match.group(1))
            n_total = int(match.group(2))
            n_pending = int(pending.split("=", 1)[1])
        return ArchiverInfo(name=archiver,
                            state=state,
                            total=n_total,
                            nok=n_nok,
                            pending=n_pending,
                            error=error)

    def get_archiver_infos(self, archiver_status):
        archiver_statuses = (self._parse_archiver_status(status)
                             for status in archiver_status)
        return archiver_statuses

    async def get_archiver_attribute_info(self, archiver):

        """
        Return a AttributeInfo object for each configured attribute,
        along with some statistics
        """

        archiver_info_attr_names = [
            "AttributeList",
            "AttributeErrorList",
            "AttributeEventNumberList",
            "AttributePendingList",
            "AttributeRecordFreqList",
            "AttributeStartedList",
            "AttributeStoppedList",
            "AttributePausedList",
            "AttributeNOKList",

            "AttributeNumber",
            "AttributeNokNumber",
            "AttributeStartedNumber",
            "AttributeStoppedNumber",
            "AttributePausedNumber"
        ]
        proxy = self.get_archiver_proxy(archiver)
        attr_data = await proxy.read_attributes(archiver_info_attr_names)
        info_attrs = {
            att.name: att.value
            for att in attr_data
        }

        def safe_list(maybe_seq):
            "Tango tends to return None instead of an empty list"
            if maybe_seq is None:
                return []
            return maybe_seq

        attr_names = safe_list(info_attrs["AttributeList"])
        # These attributes just list attribute names
        started_attrs = set(safe_list(info_attrs["AttributeStartedList"]))
        stopped_attrs = set(safe_list(info_attrs["AttributeStoppedList"]))
        paused_attrs = set(safe_list(info_attrs["AttributePausedList"]))
        nok_attrs = set(safe_list(info_attrs["AttributeNokList"]))

        def safe_getitem(index, seq, default):
            "The archiver does not always return lists the same length"
            seq = safe_list(seq)
            if index >= len(seq):
                return default
            return seq[index]

        attribute_infos = [
            AttributeInfo(
                name=name,
                # These attributes are lists in the same order as the AttributeList
                error=safe_getitem(i, info_attrs["AttributeErrorList"], ""),
                events=safe_getitem(i, info_attrs["AttributeEventNumberList"], 0),
                pending=safe_getitem(i, info_attrs["AttributePendingList"], 0),
                record_freq=safe_getitem(i, info_attrs["AttributeRecordFreqList"], 0),
                started=(name in started_attrs),
                stopped=(name in stopped_attrs),
                paused=(name in paused_attrs),
                nok=(name in nok_attrs),
            )
            for i, name in enumerate(attr_names)
        ]

        # These are single numbers
        n_attrs = info_attrs["AttributeNumber"]
        n_nok = info_attrs["AttributeNokNumber"]
        n_stopped = info_attrs["AttributeStoppedNumber"]
        n_paused = info_attrs["AttributePausedNumber"]

        return attribute_infos, n_attrs, n_nok, n_stopped, n_paused

    async def get_attribute_status(self, attribute, archiver):
        """
        Example of the value of AttributeStatus:
        ----
        Event status       : Event received
        Events engine      : ZMQ
        Archiving          : Started
        Event OK counter   : 1732 - 2021-09-18 17:44:54.241246
        Event NOK counter  : 2558 - 2021-09-18 17:35:20.528036
        DB ERRORS counter  : 0 - YYYY-MM-DD HH:MM:SS.UUUUUU
        Storing time AVG   : 0.009101s
        Processing time AVG: 0.009187s
        ----
        """
        proxy = self.get_archiver_proxy(archiver)
        status_str = await proxy.command_inout("AttributeStatus", attribute)
        return [
            [s.strip() for s in line.split(":", 1)]
            for line in status_str.splitlines()
            if line.strip()  # Apparently there may be empty lines in the status...
        ]

    async def get_attribute_config(self, attribute):
        proxy = await DeviceProxy(attribute)
        return await proxy.info()

    async def stop_archiver(self, archiver):
        proxy = self.get_archiver_proxy(archiver)
        await proxy.command_inout("Stop")

    async def start_archiver(self, archiver):
        proxy = self.get_archiver_proxy(archiver)
        await proxy.command_inout("Start")
