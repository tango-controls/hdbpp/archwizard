from functools import lru_cache
from typing import Optional, List, Dict

from pydantic import BaseSettings
import tango


class Settings(BaseSettings):

    app_name: str = "ArchWizard"
    port: int = 5007

    hdb_configuration_managers: str = None

    class Config:
        env_file = ".env"

    def get_managers(self):

        if self.hdb_configuration_managers is not None:
            print("Got configuration from env:", self.hdb_configuration_managers)
            return dict(manager.strip().split("=", 1)
                        for manager in self.hdb_configuration_managers.strip().split(","))

        try:
            print("Trying to find an archiving manager in the local control system...")
            db = tango.Database()
            db_host = db.get_db_host()
            db_port = db.get_db_port()
            hdb_managers = db.get_device_exported_for_class("HdbConfigurationManager")

            if hdb_managers:
                print("...found", hdb_managers)
                print("Using first one")
                url_prefix = f"tango://{ db_host }:{ db_port }/"
                default_manager_device = url_prefix + hdb_managers[0]
                return {"default": default_manager_device}
        except tango.DevFailed:
            pass
        return {}


@lru_cache(1)
def get_config():
    print("Loading configuration...")
    return Settings()
