from datetime import datetime
import logging
from typing import Optional, List
from operator import attrgetter
from urllib.parse import quote_plus

from aiocache import cached
from fastapi import FastAPI, Request, Query
from fastapi.responses import RedirectResponse, HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
import tango
from tango.asyncio import AttributeProxy

from .config import get_config
from .hdbpp import HdbppConfig, shorten_archiver_name, shorten_attribute_name


app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")
templates.env.zip = zip
templates.env.filters['quote_plus'] = lambda u: quote_plus(u)
templates.env.filters['shorten_archiver_name'] = shorten_archiver_name
templates.env.filters['shorten_attribute_name'] = shorten_attribute_name


@cached()
async def get_hdbpp(manager: str):
    "Get a config object for the configured HDB++ manager device"
    config = get_config()
    logging.info("Config: %r", config)
    device = config.get_managers()[manager]
    if device:
        logging.debug("Found manager device for %r: %r", manager, device)
        hdbpp = HdbppConfig(device)
        await hdbpp.setup()
        logging.info("Connected to manager device %r for %r", manager, device)
        return hdbpp
    else:
        logging.error("No manager device found for %r", manager)


@app.exception_handler(tango.DevFailed)
async def unicorn_exception_handler(request: Request, exc: tango.DevFailed):
    """Generic handler of unhandled Tango related errors"""

    # The idea is that it's better to "leak" these exceptions to the user
    # than to end up in a "internal error" or similar broken state.
    # If there's a problem talking to a device there is not much we can do
    # here anyway.

    logging.exception("Unhandled tango DevFailed exception")
    return HTMLResponse(f"<h2>Tango error</h2> <pre>{exc}</pre>")


@app.get("/")
async def show_index(request: Request):

    managers = get_config().get_managers()

    return templates.TemplateResponse(
        "index.html.jinja2",
        {
            "request": request,
            "managers": managers
        })


def check_only(att, f):
    if f.startswith("!"):
        value = getattr(att, f[1:])
        return not bool(value)
    else:
        value = getattr(att, f)
        return bool(value)


def check_filter(filter_, att):
    if filter_:
        return filter_ in att.name
    return True


@app.get("/manager/{manager}")
async def show_manager(
        manager: str,
        request: Request,
        attr_search: str = None,
        filter: Optional[str] = None,
        only: List[str] = Query([]),
        sort: Optional[str] = None,
        sort_rev: bool = False,
        clear: Optional[bool] = False):

    if clear:
        # Drop all the filtering
        url = app.url_path_for("show_manager", manager=manager)
        return RedirectResponse(url=url)

    hdbpp = await get_hdbpp(manager)

    if attr_search:
        search_results = await hdbpp.search_attributes(attr_search)
    else:
        search_results = []

    manager_attributes = await hdbpp.get_manager_info_attributes()
    archivers = hdbpp.get_archiver_infos(manager_attributes["ArchiverStatus"])

    filtered_archivers = (arch for arch in archivers
                          if all(check_only(arch, o) for o in only)
                          and check_filter(filter, arch))

    if sort:
        filtered_archivers = sorted(filtered_archivers,
                                    key=attrgetter(sort),
                                    reverse=sort_rev)

    accept = request.headers.get("accept", "").split(",")
    if accept and accept[0] == "application/json":
        return {
            "manager": manager,
            "attributes": search_results
        }

    return templates.TemplateResponse(
        "manager.html.jinja2",
        {
            "manager": manager,
            "attributes": manager_attributes,
            "attr_search": attr_search,
            "search_results": search_results,
            "archivers": filtered_archivers,
            "filter": filter,
            "only": only,
            "sort": sort,
            "sort_rev": sort_rev,
            "request": request,
            "hdbpp": hdbpp,
            "now": datetime.now()
        })


@app.get("/manager/{manager}/archiver/")
async def show_archiver(request: Request,
                        manager: str,
                        archiver: Optional[str] = None,
                        attribute: Optional[str] = None,
                        only: List[str] = Query([]),  # Checkboxes
                        attr_filter: Optional[str] = None,  # Name filter
                        sort: Optional[str] = None,
                        sort_rev: bool = False,
                        clear: Optional[bool] = False):

    if clear:
        # Drop all the filtering
        url = (app.url_path_for("show_archiver", manager=manager)
               + f"?archiver={quote_plus(archiver)}")
        return RedirectResponse(url=url)

    hdbpp = await get_hdbpp(manager)
    if attribute and not archiver:
        archiver = await hdbpp.get_archiver_for_attribute(attribute)
    logging.debug("Manager: %r, archiver: %r", manager, archiver)
    device_status = await hdbpp.get_archiver_status(archiver)
    attributes, n_attrs, n_nok, n_stopped, n_paused = await hdbpp.get_archiver_attribute_info(archiver)

    # Get device info, conveniently spread out over two calls...
    full_info = await hdbpp.get_device_info(archiver)
    archiver_proxy = hdbpp.get_archiver_proxy(archiver)
    info = archiver_proxy.info()

    filtered_attributes = (att for att in attributes
                           if all(check_only(att, o) for o in only)
                           and check_filter(attr_filter, att))

    if sort:
        filtered_attributes = sorted(filtered_attributes,
                                     key=attrgetter(sort),
                                     reverse=sort_rev)

    return templates.TemplateResponse(
        "archiver.html.jinja2",
        {
            "manager": manager,
            "archiver": archiver,
            "status": device_status,
            "info": info,
            "full_info": full_info,
            "n_attrs": n_attrs,
            "n_nok": n_nok,
            "n_stopped": n_stopped,
            "n_paused": n_paused,
            "attributes": filtered_attributes,
            "filter": attr_filter,
            "only": only,
            "sort": sort,
            "sort_rev": sort_rev,
            "request": request,
            "hdbpp": hdbpp,
            "now": datetime.now()
        })


def parse_polling_status(value):
    polling_status = value.split("\n", 6)
    attr_part, *data = polling_status
    attr = attr_part.split("=", 1)[1].strip().lower()
    if len(data) == 5:
        data.append("")
    return attr, data


@app.get("/manager/{manager}/attribute/")
async def show_attribute(manager: str,
                         attribute: str,
                         request: Request):

    """Show useful info about the given attribute"""

    # name = name.lower()
    # device = "/".join([domain, family, member]).lower()
    # attribute = "/".join([device, name])
    hdbpp = await get_hdbpp(manager)
    try:
        proxy = await AttributeProxy(attribute)
        config = await proxy.get_config()
        data_type = str(tango.CmdArgType.values[config.data_type])

        device_proxy = proxy.get_device_proxy()
        info = device_proxy.info()
        polling_status_all = dict(parse_polling_status(s)
                              for s in device_proxy.polling_status() if s)
        polling_status = polling_status_all.get(proxy.name())
        device = device_proxy.name()
        full_info = await hdbpp.get_device_info(device)
    except tango.DevFailed as e:
        config = None
        data_type = None
        info = None
        polling_status = None
        device = None
        full_info = None
        error = str(e)
    else:
        error = None
    archiver = await hdbpp.get_archiver_for_attribute(attribute)
    archiving_status = await hdbpp.get_attribute_status(attribute, archiver)

    return templates.TemplateResponse("attribute.html.jinja2", {
        "request": request,
        "manager": manager,
        "attribute": attribute,
        "device": device,
        "archiver": archiver,
        "info": info,
        "full_info": full_info,
        "config": config,
        "data_type": data_type,
        "polling_status": polling_status,
        "archiving_status": archiving_status,
        "now": datetime.now(),
        "error": str(error)
    })

    # except tango.DevFailed as e:
    #     return HTMLResponse(f"Could not get data for {attribute}! <pre>{e}</pre>")


@app.get("/manager/{manager}/action/{action}/")
async def take_action(request: Request,
                      manager: str,
                      action: str,
                      archiver: str,
                      attribute: List[str] = Query([])):

    hdbpp = await get_hdbpp(manager)
    if action == "reset":
        await hdbpp.manager.command_inout("ResetStatistics")  # Start counters from zero,
    else:
        if attribute:
            for name in attribute:
                if action == "stop":
                    await hdbpp.manager.command_inout("AttributeStop", name)
                elif action == "start":
                    await hdbpp.manager.command_inout("AttributeStart", name)
                elif action == "pause":
                    await hdbpp.manager.command_inout("AttributePause", name)
                else:
                    raise ValueError(f"'{action}' is not a valid attribute action.")
        else:
            if action == "stop":
                await hdbpp.stop_archiver(archiver)
            elif action == "start":
                await hdbpp.start_archiver(archiver)
            else:
                raise ValueError(f"'{action}' is not a valid archiver action.")

    return RedirectResponse(url=app.url_path_for("show_archiver", manager=manager)
                            + f"?archiver={quote_plus(archiver)}")
