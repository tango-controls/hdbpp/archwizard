FROM harbor.maxiv.lu.se/dockerhub/library/python:3.10-slim

ENV DEBIAN_FRONTEND noninteractive
RUN ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime

RUN groupadd -r -g 1000 kits \
  && useradd --no-log-init -r -g kits -u 1000 kits

COPY --chown=kits:kits . /app

EXPOSE 8000
WORKDIR /app

RUN python -m venv env
RUN env/bin/pip install -v -e .
RUN ls env/bin
CMD env/bin/uvicorn archwizard.main:app --host 0.0.0.0 --port 8000 --forwarded-allow-ips=* --proxy-headers
USER kits
